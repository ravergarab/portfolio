import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { SideNavComponentComponent } from './side-nav-component/side-nav-component.component';

import { ChartsModule } from 'ng2-charts';
import { SideNavContentComponent } from './side-nav-content/side-nav-content.component';
import { ChartComponent } from './chart/chart.component';
import { ReadFileComponent } from './read-file/read-file.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { SudokuSolverComponent } from './sudoku-solver/sudoku-solver.component';
import { DataAnalyticsComponentComponent } from './data-analytics-component/data-analytics-component.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';

import { MatIconModule } from "@angular/material/icon";
import { HttpClientModule } from "@angular/common/http";
import { TableComponent } from './table/table.component';



@NgModule({
  declarations: [
    AppComponent,
    SideNavComponentComponent,
    SideNavContentComponent,
    ChartComponent,
    ReadFileComponent,
    SudokuSolverComponent,
    DataAnalyticsComponentComponent,
    WelcomePageComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    TooltipModule.forRoot(),
    CarouselModule.forRoot(),
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
