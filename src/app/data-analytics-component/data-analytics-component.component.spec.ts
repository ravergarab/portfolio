import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAnalyticsComponentComponent } from './data-analytics-component.component';

describe('DataAnalyticsComponentComponent', () => {
  let component: DataAnalyticsComponentComponent;
  let fixture: ComponentFixture<DataAnalyticsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAnalyticsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAnalyticsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
