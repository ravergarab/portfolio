/**
 * This module just combines all the data analytics tools in one component
 */
import { Component, OnInit } from '@angular/core';
import { LineChartDataWithLabels } from '../chart/char.data.format';

@Component({
  selector: 'app-data-analytics-component',
  templateUrl: './data-analytics-component.component.html',
  styleUrls: ['./data-analytics-component.component.css']
})
export class DataAnalyticsComponentComponent implements OnInit {
  /** Here is the usable dataset */
  salesDataSet: SalesDataSet[];
  /** Here is the filtered dataset (what the user views in charts and tables) */
  currentChartData: LineChartDataWithLabels;
  /** UI Bools */
  showReadFile = true;
  showTable = false;
  constructor() { }

  ngOnInit() {
  }

  /** Triggered when the file is parsed */
  setSalesDataset(event: SalesDataSet[]): void {
    console.log(event);
    this.salesDataSet = event;
    this.showReadFile = false;
  }

  /** Triggered when the user changes the displayed data for the charts */
  setCurrentChartData(event: LineChartDataWithLabels): void {
    this.currentChartData = event;
  }
}
