interface SalesDataSet {
    CustomerID: string,
    AmountOnTransaction: number,
    Product: string,
    Date: Date
}