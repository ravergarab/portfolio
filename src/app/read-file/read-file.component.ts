import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSelectChange, MatSnackBar } from '@angular/material';
import { FormArray, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-read-file',
  templateUrl: './read-file.component.html',
  styleUrls: ['./read-file.component.css']
})
export class ReadFileComponent implements OnInit {
  
  /** CONSTANTS */
  readonly PREVIEW_ROWS = 7;
  readonly OK_STRING = "OK";

  /** User Bools */
  warning: boolean;
  warningText: string;

  rawFile:any;
  filePreview: string[][];
  fileParsed: string[][];

  dataHasHeaders = true;
  headerOptions = [
    "CustomerID",
    "AmountOnTransaction",
    "Product",
    "Date"
  ]

  /** Here we keep track of the column selection */
  headerSelectionForm = new FormGroup({
    headerSelectArray: new FormArray([])
  });

  /** This var keeps track of the headers selected by the user */
  headerIndex = {
    CustomerID: -1,
    AmountOnTransaction: -1,
    Product: -1,
    Date: -1
  }
  
  
  selectedHeader: string;
  
  @Output() salesDataSetComplete: EventEmitter<SalesDataSet[]> = new EventEmitter<SalesDataSet[]>();
  constructor(public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  /** GETTERS */
  get headerSelectionArray() {
    return this.headerSelectionForm.get('headerSelectArray') as FormArray;
  }

  fileChanged(event: any) {
      this.rawFile = event.target.files[0];
      this.populateFilePreview();
  }

  /**
   * It parses the file and returns the first few columns
   */
  private populateFilePreview(){
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      // TODO: for now, just only doing CSVs - And move to a not embedded method
      const isCsv = true;
      if (isCsv){
        this.fileParsed = this.parseCsvFile(fileReader.result as string);
      }
      // Here we set the preview!
      this.filePreview = [];
      for (let i = 0 ; i < Math.min(this.fileParsed.length, this.PREVIEW_ROWS); i++){
        this.filePreview.push(this.fileParsed[i]);
      }
    }    
    fileReader.readAsText(this.rawFile);
  }


  /**
   * Parses the CSV file!
   * @param csvFile raw string of a csv file
   */
  private parseCsvFile(csvFile: string): string[][]{
    // For now, return a 2D array... should move to objs (or maybe after selecting cols)
    const rows = csvFile.split("\n");
    let csvFileArray = [];
    rows.forEach((row, index) => {
      csvFileArray.push(row.split(","));
      // Make sure we keep track of the headers!
      if (index == 0){
        for (let col of csvFileArray[0]){
          this.headerSelectionArray.push(new FormControl(''));
        }
      }
    })
    return csvFileArray;
  }

  /**
   * Here we handle the event forms
   */
  headerDefinitionChange(event: MatSelectChange, columnIndex: number): void {

    if (event.value){
      // Here we check to see if that header was assigned to a different column
      if (this.headerIndex[event.value] > -1){
        // We first clear the old selection - Also, we access the controls using index!
        this.headerSelectionArray.controls[this.headerIndex[event.value]].setValue("");
        // Let the user know!
        this.openSnackBar("Column selected twice, removing old selection", this.OK_STRING);
      }      
      // We just assign the col index!
      this.headerIndex[event.value] = columnIndex;
      // Re-assigning the value in case we deleted it!      
      this.headerSelectionArray.controls[this.headerIndex[event.value]].setValue(event.value);
    } else {
      // Clearing the column selection!
      Object.keys(this.headerIndex).forEach(key =>{
        if (this.headerIndex[key] == columnIndex){
          this.headerIndex[key] = -1;
        }
      });
    }    
    
    // console.log(this.headerSelectionArray);
  }

  /** It passes the dataset to the chart widget! */
  pushDataset():void {
    const salesDataSet = this.getSalesDataset();
    if (this.validDataSet()){
      this.salesDataSetComplete.emit(salesDataSet);
    }
  }

  /**
   * It displays the warning message 
   * @param warningText Message to display the user
   */
  private setWarning(warningText: string): void {
    this.warningText = warningText;
    this.warning = true;
  }

  /** Check whether we have a dataset with all the required columns! */
  private validDataSet() : boolean {
    let validDataSet = true;
    let errorMessage = "Missing Columns ";
    for (let key of Object.keys(this.headerIndex)){
      if (this.headerIndex[key] == -1){
        errorMessage += key + " ";
        validDataSet = false;
      }
    }

    if (!validDataSet){
      this.openSnackBar(errorMessage, this.OK_STRING);
    }
    return validDataSet;
  }

  /**
   * It parses the raw file and returns only the columns we need
   */
  private getSalesDataset(): SalesDataSet[]{
    let salesDataSet: SalesDataSet[] = [];
    for (let i = 0; i < this.fileParsed.length; i++){
      if (i == 0 && this.dataHasHeaders){
        continue;
      }
      // TODO: add a date validation!!!
      let saleDataSet: SalesDataSet = {
        CustomerID: this.fileParsed[i][this.headerIndex["CustomerID"]],
        AmountOnTransaction: parseFloat(this.fileParsed[i][this.headerIndex["CustomerID"]]),
        Product: this.fileParsed[i][this.headerIndex["Product"]],
        Date: new Date(this.fileParsed[i][this.headerIndex["Date"]]),
      }
      salesDataSet.push(saleDataSet);
    }

    return salesDataSet;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
