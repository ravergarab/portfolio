import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from "@angular/platform-browser";

export interface MenuObject {
  ItemName: string;
  RouterLink: string;
  MatIcon: string;
}

@Component({
  selector: 'app-side-nav-component',
  templateUrl: './side-nav-component.component.html',
  styleUrls: ['./side-nav-component.component.css']
})

export class SideNavComponentComponent implements OnInit {

  readonly MENU_OBJECTS: MenuObject[] = [
    {ItemName: 'Home', RouterLink: '/', MatIcon: "home"},
    {ItemName: 'Sudoku', RouterLink: '/Sudoku', MatIcon: "border_all"},
    {ItemName: 'Sales Analytics', RouterLink: '/Analytics', MatIcon: "show_chart"}
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      'paddle_labs',
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icon/paddle_labs_icon.svg")
    );
   }

  ngOnInit() {
  }

}
