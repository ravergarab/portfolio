import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

/** A Sudoku Puzzle is just a 2D array of this object. */
export interface FieldProperties {  
  /** 0 Based index of the row */  
  row: number,
  /** 0 Based index of the columns */
  col: number,
  /** This refers to the ID of the 3x3 grid (1-9) */
  grid: number,
  /** This is set to true or false depending on wether the user added a value or not */
  hasInitialValue: boolean,
  /** Keep track of all the remaining values that meet the game rules for one cell */
  possibleValues: number[],
  /** The value to display (it may not be final since we iterate over many values) */
  value?: number
}

export interface CellCoordinates {
  rowIndex: number,
  colIndex: number
}

@Component({
  selector: 'app-sudoku-solver',
  templateUrl: './sudoku-solver.component.html',
  styleUrls: ['./sudoku-solver.component.css']
})
export class SudokuSolverComponent implements OnInit {

  /** CONSTANTS */
  readonly NO_SOLUTION_MESSAGE = "No solution found";
  readonly SOLUTION_FOUND_MESSAGE = "Solution found";
  readonly INVALID_NUMBER_MESSAGE = "The number just entered is not valid";
  readonly OK_ACTION = "OK";
  readonly PUZZLE_CLEARED = "Puzzle successfully cleared";
  readonly MAX_ITERATION_COUNT = 10;
  /** CONSTANTS - LOCAL STORAGE */
  readonly SUDOKU_PUZZLE_KEY = "sudokuPuzzle";

  /** The Puzzle */
  sudokuPuzzle: FieldProperties[][] = [];
  /** Simple way to get an ID for the 3x3 grids */
  gridMap: number[][] = [
    [1,1,1,2,2,2,3,3,3],
    [1,1,1,2,2,2,3,3,3],
    [1,1,1,2,2,2,3,3,3],
    [4,4,4,5,5,5,6,6,6],
    [4,4,4,5,5,5,6,6,6],
    [4,4,4,5,5,5,6,6,6],
    [7,7,7,8,8,8,9,9,9],
    [7,7,7,8,8,8,9,9,9],
    [7,7,7,8,8,8,9,9,9]
  ];

  constructor(public snackBar: MatSnackBar) {    
  }

  ngOnInit() {
    // Sets the initial values for the form
    this.initializeInputFields();
    // If we have something saves from a different instance, we load it
    this.loadPuzzle();
  }

  /** This guy Initializes the fields we use to keep track of the puzzle vars*/
  private initializeInputFields(): void {
    this.sudokuPuzzle = [];
    for (let i = 0; i < 9; i++){
      let row: FieldProperties[] = [];
      for (let j = 0 ; j < 9; j++){
        const fieldProperties: FieldProperties = {
          row: i,
          col: j,
          grid: this.gridMap[i][j],
          possibleValues: [1,2,3,4,5,6,7,8,9],
          hasInitialValue: false
        }
        row.push(fieldProperties); 
      }
      this.sudokuPuzzle.push(row);
    }
  }

  /**
   * It sets the FieldProperties.hasInitialValue property as true for a specific row/col 
   * @param rowIndex row index of the value to modify
   * @param colIndex column index of the value to modify
   */
  private setHasInitialValue(rowIndex: number, colIndex: number): void {
    let thisField = this.sudokuPuzzle[rowIndex][colIndex];
    if (thisField.value){
      if (thisField.value > 0 && thisField.value < 10){
        thisField.hasInitialValue = true;
        thisField.possibleValues = [thisField.value];
      } else {
        thisField.hasInitialValue = false;
        thisField.value = null;
        this.openSnackBar(this.INVALID_NUMBER_MESSAGE, this.OK_ACTION);
      }
    } else {
      thisField.hasInitialValue = false;
      thisField.value = null;
    }
  }


  /** Finds the first empty cell and triggers the solver! */
  public solveSudoku(): void {
    // console.log(this.sudokuPuzzle);
    this.solveUsingLogic(0);
    // If we still have empty cells after trying to use logic, we try backtracking (brut force!)
    if (this.getNumberOfEmptyCells() > 0){
      this.openSnackBar("This is a tough puzzle!", this.OK_ACTION);
      // Here we just do brut force!
      this.solveUsingBrutForce(0,0);
    }
    
  }


  /**
   * It tries to solve as much as we can by elimination. Leaving the possible values as small
   * as possible using the only sudoku rules I know! It solves the easy and medium puzzles, but not the
   * experts only puzzles!!
   * @param iterationCount how many tries have we had at this before we give upt!
   */
  private solveUsingLogic(iterationCount: number): void {
    let nextAvailableCell: CellCoordinates;
    // Here we make sure that if we start on the first cell, we don't go to the next one right away!
    if (this.sudokuPuzzle[0][0].value){
      nextAvailableCell = this.getNextAvailableCellCoord(0, 0);
    } else {
      nextAvailableCell = {
        "colIndex": 0,
        "rowIndex": 0
      }
    }
    // If there is an available cell (haven't made it to the end of the puzzle), keep looping
    while (nextAvailableCell){      
      this.setInitialPossibleValues(nextAvailableCell.rowIndex, nextAvailableCell.colIndex, iterationCount);
      // We give the basic "3 rules" two passes to the puzzle since it is very inexpensive to compute!
      if (iterationCount > 2){
        // Here we try more advanced and expensive rules!
        this.handleAdvancedLogic();
        this.checkUniquePossibilities(nextAvailableCell.rowIndex, nextAvailableCell.colIndex);
      }
      nextAvailableCell = this.getNextAvailableCellCoord(nextAvailableCell.rowIndex, nextAvailableCell.colIndex);
    }
    // Implementing an exit condition!
    if (iterationCount < this.MAX_ITERATION_COUNT){
      if (this.getNextAvailableCellCoord(0,0)){
        this.solveUsingLogic(iterationCount + 1);
      } else {
        this.openSnackBar("Puzzle Solved!", this.OK_ACTION);
      }
    }
  }

  
  /**
   * Here we start looking for a solution using backtracking.
   * It should be a lot quicker than the traditional one since we have eliminated
   * many solutions!
   * @param rowIndex initial row to start looking for values!
   * @param colIndex initial col to start iterating!
   */
  private solveUsingBrutForce(rowIndex: number, colIndex: number): boolean {
    let nextCoordinates = this.getNextAvailableCellCoord(rowIndex, colIndex)
    // Here we just make sure we start iterating on the first cell
    if (rowIndex == 0 && colIndex == 0){
      if (this.sudokuPuzzle[rowIndex][colIndex].possibleValues.length == 1){
        rowIndex = nextCoordinates.rowIndex;
        colIndex = nextCoordinates.colIndex;
        nextCoordinates = this.getNextAvailableCellCoord(rowIndex, colIndex);
      }
    }
    // Now we try to plug one of the possible values in the  first available cell
    for (let possibleValue of this.sudokuPuzzle[rowIndex][colIndex].possibleValues){
      if (this.isNewNumberValid(possibleValue, rowIndex, colIndex)){
        this.sudokuPuzzle[rowIndex][colIndex].value = possibleValue;
        // If we don't have new coordinates, we finished the puzzle!
        if (!nextCoordinates){
          return true;
        }
        // If the next value is true (recursively) we found a solution!
        if (this.solveUsingBrutForce(nextCoordinates.rowIndex, nextCoordinates.colIndex)){
          return true;
        } else {
          // Clear non-solution values
          this.sudokuPuzzle[nextCoordinates.rowIndex][nextCoordinates.colIndex].value = null;
        }
      }
    }
    return false;
  }

  /**
   * It assigns a set of possible values based on the sudoku rules
   * @param rowIndex index of the field
   * @param colIndex col index of the field 
   */
  private setInitialPossibleValues(rowIndex: number, colIndex: number, iterationCount: number): void {
    let currentCell = this.sudokuPuzzle[rowIndex][colIndex];
    for (let i = 1; i <= 9; i++){
      if (this.isNewNumberValid(i, rowIndex, colIndex)){
        // we only add on the first iteration!
        if (iterationCount == 0){
          if (!currentCell.possibleValues.includes(i)){
            currentCell.possibleValues.push(i);
          }
        }
      } else {
        // Remove this number if it no longer valid!
        currentCell.possibleValues = currentCell.possibleValues.filter(x => x != i);
      }
    }
    // If there is only one possible value, set it!
    if (currentCell.possibleValues.length == 1){
      currentCell.value = currentCell.possibleValues[0];
    }
  }

  /**
   * This basically takes care of the scenario when we have a number
   * that must be in a row or col (within a 3x3 grid) but we don't
   * know exactly where, but it can be eliminated from the other grids
   * because we know it MUST be in this row or col...
   */
  handleAdvancedLogic(): void {
    // We loop through each grid first!
    for (let gridIndex = 1; gridIndex <= 9 ; gridIndex++){
      // Now we loop for every sudoku allowable number
      for (let i=1; i <=9; i++){
        let colFound: number[] = [];
        let rowFound: number[] = [];
        this.sudokuPuzzle.forEach(row => {
          row.forEach(col => {
            if (col.grid == gridIndex){ // && this.canCellBeEdited(col)
              if (col.possibleValues.includes(i)){
                if (!colFound.includes(col.col)){
                  colFound.push(col.col);
                }
                if (!rowFound.includes(col.row)){
                  rowFound.push(col.row);
                }
              }
            }
          })
        });
        // Here we check whether the number can only be in one row or col!
        if (colFound.length == 1){
          // this means it MUST be on this col (so we can exclude this value from other grids)
          this.excludeValueFromOtherGrids(colFound[0], gridIndex, i, false);
        }
        if (rowFound.length == 1){
          // this value MUST be on this row at this grid, so we can exclude it from other grids!
          this.excludeValueFromOtherGrids(rowFound[0], gridIndex, i, true);
        }
      }
    }
  }


  /**
   * Here we go back to the grid and check if the a possible found ONLY in one cell
   * For example, if both, 5 and 9 can go in a cell, but 5 can only be in that cell relative to 
   * the grid/row or col, we fit 5 in that cell!
   */
  private checkUniquePossibilities(rowIndex: number, colIndex: number): void {

    const thisGrid = this.sudokuPuzzle[rowIndex][colIndex].grid  

    // We loop through each possible value
    for (let possibleValue = 1; possibleValue <= 9; possibleValue++){
      // let's go over the row
      let timesFound = 0;
      let indexWhereFound = 0
      for (let i = 0; i < this.sudokuPuzzle[rowIndex].length; i++){
        if (this.sudokuPuzzle[rowIndex][i].possibleValues.includes(possibleValue)){
          // Every time we find this possible value in the row, we add to the counter
          timesFound++;
          indexWhereFound = i;
        }        
      }
      // If this possible value was only found once, we assign it to that cell!
      if (timesFound == 1){        
        this.sudokuPuzzle[rowIndex][indexWhereFound].possibleValues = [possibleValue];
        this.sudokuPuzzle[rowIndex][indexWhereFound].value = possibleValue;
      }
      // No we repeat this with the cols!
      timesFound = 0;
      indexWhereFound = 0
      for (let i = 0; i < this.sudokuPuzzle.length; i++){
        if (this.sudokuPuzzle[i][colIndex].possibleValues.includes(possibleValue)){
          // Every time we find this possible value in the row, we add to the counter
          timesFound++;
          indexWhereFound = i;
        }        
      }
      // If this possible value was only found once, we assign it to that cell!
      if (timesFound == 1){        
        this.sudokuPuzzle[indexWhereFound][colIndex].possibleValues = [possibleValue];
        this.sudokuPuzzle[indexWhereFound][colIndex].value = possibleValue;
      }
      // Now we do this for 3x3 arrays
      timesFound = 0;
      let rowWhereFound = 0;
      let colWhereFound = 0;
      for (let i = 0; i < this.sudokuPuzzle.length; i++){
        for (let j=0; j < this.sudokuPuzzle[i].length; j++){
          if (this.sudokuPuzzle[i][j].grid == thisGrid){
            if (this.sudokuPuzzle[i][j].possibleValues.includes(possibleValue)){
              // Every time we find this possible value in the row, we add to the counter
              timesFound++;
              rowWhereFound = i;
              colWhereFound = j;
            }
          }
        }
      }
      if (timesFound == 1){        
        this.sudokuPuzzle[rowWhereFound][colWhereFound].possibleValues = [possibleValue];
        this.sudokuPuzzle[rowWhereFound][colWhereFound].value = possibleValue;
      }

    }    
  }



  /**
   * This rule is as follow, if a value is found on two cells in a line within a 3x3 grid, we
   * know it has to be on that line, so we can safely exclude it from the other 3x3 grids
   * @param lineIndex either col or row index, depending on what we are excluding
   * @param gridIndex the index of the grid that originated this
   * @param valueToExclude number we need to exclude
   * @param isRow if true, will exclude on rows, otherwise, we'll use columns
   */
  private excludeValueFromOtherGrids(lineIndex: number, gridIndex: number, valueToExclude: number, isRow: boolean): void {
    // We get all the affected cells and delete the values in the row/col
    if (isRow){
      for (let i = 0; i < this.sudokuPuzzle[lineIndex].length; i++){
        if (this.sudokuPuzzle[lineIndex][i].grid != gridIndex){
          // Now we just remove that value!
          this.sudokuPuzzle[lineIndex][i].possibleValues = this.sudokuPuzzle[lineIndex][i].possibleValues.filter(x => x != valueToExclude);
          if (this.sudokuPuzzle[lineIndex][i].possibleValues.length == 1){
            this.sudokuPuzzle[lineIndex][i].value == this.sudokuPuzzle[lineIndex][i].possibleValues[0];
          }
        }
      }
    } else {
      for (let i = 0; i < this.sudokuPuzzle.length; i++){
        if (this.sudokuPuzzle[i][lineIndex].grid != gridIndex){
          // Now we just remove that value!
          this.sudokuPuzzle[i][lineIndex].possibleValues =this.sudokuPuzzle[i][lineIndex].possibleValues.filter(x => x != valueToExclude);
          if (this.sudokuPuzzle[i][lineIndex].possibleValues.length == 1){
            this.sudokuPuzzle[i][lineIndex].value == this.sudokuPuzzle[i][lineIndex].possibleValues[0];
          }
        }        
      }
    }
  }

  /**
   * We check whether the new number can be used in this cell or not
   * by comparing against rows, cols and "3x3 grids"
   * @param newNumber Number to try in the cell
   */
  private isNewNumberValid(newNumber: number, rowIndex: number, colIndex: number): boolean {
    // First, let's check the row!
    const foundInRow = this.sudokuPuzzle[rowIndex].find(x => x.value == newNumber);
    if (foundInRow){
      return false;
    }
    // Now we checl the cols!
    const foundInCol = this.sudokuPuzzle.find(x => x[colIndex].value == newNumber);
    if (foundInCol){
      return false;
    }

    // Now we check the 3x3 grid!
    const gridIndex = this.gridMap[rowIndex][colIndex];
    for (let row of this.sudokuPuzzle){
      // We get the grid
      const foundInGridRow = row.find(x => x.grid == gridIndex && x.value == newNumber);
      if (foundInGridRow){
        return false;
      }
    }
    return true;
  }

  /**
   * It returns the row and col index for the next available cell. It returns null
   * if we've met exit condition!
   * @param rowIndex 
   * @param colIndex 
   */
  private getNextAvailableCellCoord(rowIndex: number, colIndex: number): CellCoordinates{
    let nextRowIndex: number;
    let nextColIndex: number;
    // Need to know if we move to the next row first column or same row next column
    if (colIndex == this.sudokuPuzzle[rowIndex].length - 1){
      // We need to go to the next row!
      nextRowIndex = rowIndex + 1;
      nextColIndex = 0;
      if (nextRowIndex >= this.sudokuPuzzle.length){
        return null        
      }
    } else {
      // We stay on the same row -> new col!
      nextRowIndex = rowIndex;
      nextColIndex = colIndex + 1;
    }
    // Now we check if the (row,col) indexes have a value
    if (!this.canCellBeEdited(this.sudokuPuzzle[nextRowIndex][nextColIndex])){
      // We move on to the next cell
      return this.getNextAvailableCellCoord(nextRowIndex, nextColIndex);
    }
    return {
        "rowIndex": nextRowIndex,
        "colIndex": nextColIndex
      }    
  }

  /**
   * Easy way to determine whether we can modify this cell or not based on initial valu
   * or having only one value that applies
   * @param cell A field of the sudoku puzzle
   */
  private canCellBeEdited(cell: FieldProperties): boolean {
    return Boolean(!cell.value)
  }

  /**
   * I just get a count of empty cells. Mostly used to determine wether
   * I'm making any changes to the puzzle after some iterations!
   */
  private getNumberOfEmptyCells(): number {
    let numberOfEmptyCells = 0;
    for (let i = 0; i < this.sudokuPuzzle.length; i++){
      for (let j=0;j < this.sudokuPuzzle[i].length; j++){
        if (!this.sudokuPuzzle[i][j].value){
          numberOfEmptyCells++;
        }
      }
    }
    return numberOfEmptyCells;
  }


  //#region UI/UX related methods

  /**
   * To Keep the user in the known, we show this snackbar 
   * @param message message to display
   * @param action 
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  /** It writes the puzzle into the browsers memory */
  savePuzzle(): void {
    localStorage.setItem(this.SUDOKU_PUZZLE_KEY, JSON.stringify(this.sudokuPuzzle));
  }

  /** It trieds to find the puzzle in the local storage */
  loadPuzzle(): void {
    const puzzleFromLocalStorage =  JSON.parse(localStorage.getItem(this.SUDOKU_PUZZLE_KEY));
    if (puzzleFromLocalStorage){
      this.sudokuPuzzle = puzzleFromLocalStorage as FieldProperties[][];
    }
  }


  /**
   * Handles the blur event on the fields of the sudoku puzzle
   * @param rowIndex index of the row
   * @param colIndex index of the col
   */
  public fieldBlur(rowIndex: number, colIndex: number): void {
    this.setHasInitialValue(rowIndex, colIndex);
  }

  /**
   * Sets the puzzle back to the original state!
   */
  public clearPuzzle(){
    this.initializeInputFields();
    this.openSnackBar(this.PUZZLE_CLEARED, this.OK_ACTION);
  }

  //#endregion



}
