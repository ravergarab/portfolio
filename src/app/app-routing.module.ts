import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SudokuSolverComponent } from './sudoku-solver/sudoku-solver.component';
import { DataAnalyticsComponentComponent } from './data-analytics-component/data-analytics-component.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';

const routes: Routes = [  
  { path: 'Sudoku', component: SudokuSolverComponent },
  { path: 'Analytics', component: DataAnalyticsComponentComponent },
  { path: '', component: WelcomePageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
