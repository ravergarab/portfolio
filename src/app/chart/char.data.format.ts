import { AppConstants } from '../Util/Constants';

/**
 * This class uses the SalesDataSet (or any dataset) and returns the data
 * in the format needed for all different chars as well as different data groupings
 */

export interface LineChartData {
  data: number[],
  label: string
}

export interface LineChartDataWithLabels {
  lineChartData: LineChartData[],
  lineChartLabels: string[]
}

export class ConvertData {
  /**
   * It grabs the adapted sales dataset converts it into the dataset 
   * that we'll for charts use grouped by month. It returns the amount of sales ($)
   * @param salesDataSet The sales dataset from the user
   * @param getCount If true, it will return the count, not the sum of money!
   * @param year Which year we'll look at! If not provided, we'll return current year!
   */
  public static getYearlySalesByMonth(salesDataSet: SalesDataSet[], getCount: boolean, year?: number): LineChartDataWithLabels {

    let chartData:LineChartDataWithLabels = {
      lineChartData: [],
      lineChartLabels: AppConstants.monthNames
    };
    // First we filter the data by year!
    if (!year) {
      year = new Date().getFullYear();
    }

    // Now we loop through all months! (0 based index)
    for (let i = 0; i < 12; i++){
      const dateGroupedData = salesDataSet.filter(sale => sale.Date.getFullYear() == year && sale.Date.getMonth() == i);
      // Here we need to check if the product is already in the label
      dateGroupedData.forEach(row => {
        const seriesLabel = row.Product;
        let thisSeries = chartData.lineChartData.find(x => x.label == seriesLabel);
        if (!thisSeries){
          thisSeries = {
            data: Array(12).fill(0),
            label: seriesLabel
          }
          chartData.lineChartData.push(thisSeries);
        }
        // We check wether we sum the $ or the count
        if (getCount){
          thisSeries.data[i]++;
        } else {
          thisSeries.data[i] += row.AmountOnTransaction;
        }
      });
    }
    return chartData;
  }

  
  /**
   * It grabs the adapted sales dataset converts it into the dataset 
   * that we'll for charts use grouped by days. It returns the amount of sales ($)
   * @param salesDataSet The sales dataset from the user
   * @param getCount If true, it will return the count, not the sum of money!
   * @param year Which year we'll look at! If not provided, we'll return current year!
   * @param month: The month we need to display!
   */
  public static getMonthlySalesByDay(salesDataSet: SalesDataSet[], getCount: boolean, year: number, month: number): LineChartDataWithLabels {

    let chartData:LineChartDataWithLabels = {
      lineChartData: [],
      lineChartLabels: []
    };
    // Trying to see how many days we have in the selected month!
    const daysInMonth = new Date(year, month + 1, 0).getDate();
    // We'll start with a dataset that is filtered by year and month!
    const filteredDataset = salesDataSet.filter(sale => sale.Date.getFullYear() == year && sale.Date.getMonth() == month);
    // Here days are 1 based!
    for (let i = 1; i <= daysInMonth; i++){
      // We make sure we pass the days as labels for the charts and tables!
      chartData.lineChartLabels.push(i.toString());
      const dailyDataset = filteredDataset.filter(sale => sale.Date.getDate() == i);
      // Here we need to check if the product is already in the label
      dailyDataset.forEach(row => {
        const seriesLabel = row.Product;
        let thisSeries = chartData.lineChartData.find(x => x.label == seriesLabel);
        if (!thisSeries){
          thisSeries = {
            data: Array(daysInMonth).fill(0),
            label: seriesLabel
          }
          chartData.lineChartData.push(thisSeries);
        }
        // We check wether we sum the $ or the count
        if (getCount){
          // Don't forget it is 1 based!
          thisSeries.data[(i-1)]++;
        } else {
          thisSeries.data[(i-1)] += row.AmountOnTransaction;
        }
      });
    }

    return chartData;
  }

  
  /**
   * It grabs the adapted sales dataset converts it into the dataset 
   * that we'll for charts use grouped by days.
   * @param salesDataSet The sales dataset from the user
   * @param getCount If true, it will return the count, not the sum of money!
   * @param year Which year we'll look at! If not provided, we'll return current year!
   * @param month: The month we need to display! If month is not provided, it will return the whole year
   */
  public static groupSalesByDay(salesDataSet: SalesDataSet[], getCount: boolean, year: number, month?: number): LineChartDataWithLabels {
    let chartData:LineChartDataWithLabels = {
      lineChartData: [],
      lineChartLabels: []
    };
    // Just getting the initial dataset filtered to avoid having to filter through the whole thing for every day!
    let filteredDataset = salesDataSet.filter(sale => sale.Date.getFullYear() == year);
    // Month could be 0, which would also return false, so need to be explicit
    if (month != null){
      filteredDataset = filteredDataset.filter(sale => sale.Date.getMonth() == month);
    }
    // Loop through the days of the week!
    for (let i = 0; i < 7; i++){
      // We make sure we pass the days as labels for the charts and tables!
      chartData.lineChartLabels.push(AppConstants.daysOfTheWeek[i]);
      const dailyDataset = filteredDataset.filter(sale => sale.Date.getDay() == i);
      // Here we need to check if the product is already in the label
      dailyDataset.forEach(row => {
        const seriesLabel = row.Product;
        let thisSeries = chartData.lineChartData.find(x => x.label == seriesLabel);
        if (!thisSeries){
          thisSeries = {
            data: Array(7).fill(0),
            label: seriesLabel
          }
          chartData.lineChartData.push(thisSeries);
        }
        // We check wether we sum the $ or the count
        if (getCount){
          thisSeries.data[i]++;
        } else {
          thisSeries.data[i] += row.AmountOnTransaction;
        }
      });
    }
    return chartData;
  }



  
}