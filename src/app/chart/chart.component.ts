import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { ConvertData, LineChartDataWithLabels } from './char.data.format';
import { AppConstants } from '../Util/Constants';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  // TODO: Handle missing grouping data (or just allow found year/month, etc)
  // TODO: Group by hour of day, etc...
  @Input() salesDataSet: SalesDataSet[];
  @Output() currentChartData: EventEmitter<LineChartDataWithLabels> = new EventEmitter<LineChartDataWithLabels>();
  @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  
  /** Constants */
  readonly availableMonths = AppConstants.monthNames;

  /** Form Values for debug - TODO: move to an init method*/
  public filteredYear: number = 2019;
  public filteredMonth: number; 
  public useSalesCount: boolean = false;
  public useDailySum: boolean = false;
  public chartLegend:boolean = true;
  public chartType:string = 'line';
  public chartTypes = [
    "line",
    "bar",
    "radar"
  ]
  public distinctYears: number[];
  public distinctMonths: number[] = [];

  /** this is the data that is actually bound to the chart */
  public chartData:Array<any>;  
  /** this is what we show on the x-axis*/
  public chartLabels:Array<any>;
  /** generic options for the chart */
  public chartOptions:any = {
    responsive: true
  };

  constructor() { }

  ngOnInit() {
    // As soon as we get the date, we convert it to 
    this.updateChart();
    this.setDistinctMonths(this.filteredYear);
    // TODO: have all these methods in an chartInit method:
    this.setDistinctYears();
  }

  /** Handles the year changed event */
  yearChanged(): void {
    this.filteredMonth = null;
    this.setDistinctMonths(this.filteredYear);
    this.updateChart();
  }

  /** Updates the chart when the user changes the filtered by or any other event */
  updateChart(): void {    
    // Trying clearing so they would refresh!
    this.chartLabels = [];
    let monthlyDataSet: LineChartDataWithLabels;
    if (this.useDailySum){
      monthlyDataSet = ConvertData.groupSalesByDay(this.salesDataSet, this.useSalesCount, this.filteredYear, this.filteredMonth);
    } else if (this.filteredMonth != null){
      monthlyDataSet = ConvertData.getMonthlySalesByDay(this.salesDataSet, this.useSalesCount, this.filteredYear, this.filteredMonth);
    } else {
      monthlyDataSet = ConvertData.getYearlySalesByMonth(this.salesDataSet, this.useSalesCount, this.filteredYear);
    }
    this.chartData = monthlyDataSet.lineChartData;
    this.chartLabels = monthlyDataSet.lineChartLabels;
    // Trigger the event (pushing the dataset to listeners)!
    this.currentChartData.emit(monthlyDataSet);
  }

  /**
   * It sets the Distinct Year var so that we only display those on the chart as options
   */
  private setDistinctYears(): void {
    if (this.salesDataSet){
      this.distinctYears = Array.from(new Set(this.salesDataSet.map(x=>x.Date.getFullYear())));
      this.filteredYear = Math.max(...this.distinctYears);
    }
    this.distinctYears.sort();
    return;
  }

  /**
   * Sets the available months in the dataset for a specific year!
   * @param year selected year!
   */
  private setDistinctMonths(year: number): void {
    const filteredDataset = this.salesDataSet.filter(x => x.Date.getFullYear() == year);
    this.distinctMonths = [];
    if (filteredDataset){
      this.distinctMonths = Array.from(new Set(filteredDataset.map(x=>x.Date.getMonth())));
    }
    this.distinctMonths.sort((a, b) => a - b);
    return;

  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  

}
