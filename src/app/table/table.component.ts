import { Component, OnInit, Input } from '@angular/core';
import { LineChartDataWithLabels } from '../chart/char.data.format';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input() currentChartData: LineChartDataWithLabels;
  constructor() { }

  ngOnInit() {
    console.log(this.currentChartData);
  }


}
